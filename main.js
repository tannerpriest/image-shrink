const os = require("os")
const path = require("path")
const slash = require("slash")
const imagemin = require("imagemin");
const imageminMozjpeg = require("imagemin-mozjpeg");
const imageminPngquant = require("imagemin-pngquant");
const { getMenu } = require("./assets/app/menuItems");
const { ipcMain, app, BrowserWindow, Menu, globalShortcut, shell } = require("electron");

// set environment
process.env.NODE_ENV = "production"

const isDev = process.env.NODE_ENV !== "production"
const isMac = process.platform === "darwin"

/** 
    @type {BrowserWindow}
*/

let mainWindow;
const createMainWindow = () => {
    mainWindow = new BrowserWindow({
        width: 500,
        height: 600,
        title: "Image Shrink",
        icon: `${__dirname}/assets/icons/shark.png`,
        resizable: isDev,
        backgroundColor: "#c4c4c4",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        }
    })
    if (isDev) {
        mainWindow.setFocusable(false)
    }
    mainWindow.loadURL(`file://${__dirname}/app/index.html`)
}

app.on("ready", () => {
    createMainWindow()

    const menu = getMenu(app)

    // @ts-ignore
    if (isMac) menu.unshift({ role: "appMenu" })

    const mainMenu = Menu.buildFromTemplate(menu)

    Menu.setApplicationMenu(mainMenu)

    mainWindow.on("closed", () => {
        mainWindow = null
    })

    if (isDev) {
        mainWindow.webContents.openDevTools()
    }
})

ipcMain.on("image:minimize", (e, options) => {
    options.dest = path.join(os.homedir(), "imageshrink")
    shrinkImage(options)
})

const shrinkImage = async ({ imgPath, quality, dest }) => {
    const pngQuality = quality / 100
    await imagemin([slash(imgPath)], {
        destination: dest,
        plugins: [
            imageminMozjpeg({ quality }),
            imageminPngquant({
                quality: [pngQuality, pngQuality]
            })
        ]
    }).then(files => {
        shell.openPath(dest)

        mainWindow.webContents.send("image:done")

    }).catch(err => {
        console.error("ERROR WHILE SHRINKING PHOTO: ", err)
    })
}

app.on('window-all-closed', () => {
    if (!isMac) app.quit()
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createMainWindow()
})
