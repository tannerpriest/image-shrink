const { BrowserWindow } = require("electron")

/** 
    @type {BrowserWindow}
*/

let aboutWindow;

const createAboutWindow = () => {
    aboutWindow = new BrowserWindow({
        width: 300,
        height: 600,
        title: "About Image Shrink",
        icon: `${__dirname}/assets/icons/shark.png`,
        resizable: false,
        fullscreenable: false
    })
    aboutWindow.loadFile("app/about.html")
}

module.exports = createAboutWindow