// @ts-nocheck
const { App, MenuItemConstructorOptions } = require("electron")
const createAboutWindow = require("./aboutWindow")

const isDev = process.env.NODE_ENV !== "production"
const isMac = process.platform === "darwin"

/** 
* Array of menu Items 
* @param {App} app object
* @return {Array<MenuItemConstructorOptions>}
*/

const getMenu = (app) => {
    /** 
    * @type {Array<MenuItemConstructorOptions>}
    */
    let menu = [
        {
            role: "fileMenu"
        }
        // {
        //     label: "File",
        //     submenu: [
        //         {
        //             label: "Quit",
        //             click: () => app.quit(),
        //             accelerator: "CmdOrCtrl+W"
        //         }
        //     ]
        // },
        // {
        //     label: "awesome",
        //     submenu: [
        //         {
        //             label: "wow",
        //             click: () => console.log(`clear eyes is awesome`)
        //         }
        //     ]
        // }
    ]
    if (isMac) {
        menu.unshift({
            label: app.name,
            submenu: [
                {
                    label: "About",
                    click: createAboutWindow
                }
            ]
        })
    }
    if (isDev) {
        menu.push({
            label: "Developer",
            submenu: [
                { role: "reload" },
                { role: "forceReload" },
                { role: "separator" },
                { role: "toggleDevTools" },
            ]
        })
    }

    return menu
}
module.exports = {
    getMenu
}